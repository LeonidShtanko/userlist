import { NgModule } from "@angular/core";
import { CardComponent, UserInfoComponent, HeaderComponent } from './'
import { CommonModule } from '@angular/common';
import { BirthPipe } from '../pipes/birth.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        CardComponent,
        HeaderComponent,
        UserInfoComponent,
        BirthPipe,
        
    ],
    imports: [
        FormsModule,
        CommonModule
    ],
    exports: [
        CardComponent,
        HeaderComponent,
        UserInfoComponent
    ]
})

export class ComponentsModule {}
