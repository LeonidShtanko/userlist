import { Component, Input, Output, EventEmitter } from "@angular/core";

import { User } from '../../models/user.model';
import { DataService } from '../../services/data.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    animations: [
        trigger('cardState', [
            state('void', style({ transform: 'translateX(-200px)', opacity: 0 })),
            state('normal', style({ transform: 'translateX(0)', opacity: 1 })),
            transition('void <=> normal', animate(300))
        ])
    ]
})

export class CardComponent {

    @Input() user: User;
    @Input() isEven: boolean;
    @Output() deleteEvent: EventEmitter<string> = new EventEmitter<string>();

    public isEdit: boolean;

    public animationState = 'normal';

    constructor(private dataService: DataService) { }

    public toggleEdit(): void {
        this.isEdit = !this.isEdit;
    }

    public deleteCard(): void {
        this.deleteEvent.emit(this.user.id);
    }

    public clickCard(): void {
        this.dataService.sendUserData(this.user);
    }
}
