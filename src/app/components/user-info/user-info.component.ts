import { Component, OnInit, OnDestroy } from "@angular/core";
import { User } from '../../models/user.model'; //НУЖНО ПРОПИСЫВАТЬ БЕЗ SRC
import { DataService } from '../../services/data.service';

@Component({
    selector: 'app-user-info',
    templateUrl: './user-info.component.html',
    styleUrls: ['./user-info.component.scss']
})

export class UserInfoComponent implements OnInit {

    public user: User;

    constructor(private ds: DataService) { }

    ngOnInit() {
        console.log('Component is load');
        this.ds.changeData.subscribe(data => {
            this.user = data;
        });
    }

    public getUserFromService(): void {
        // this.user = this.ds.user;
    }
}