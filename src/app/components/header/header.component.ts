import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent {

    constructor(private router: Router, private authService: AuthService) {}

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    public goTo(path: string): void {
        this.router.navigate([path]);
    }
    
    public getIsActive(path: string): boolean {
        return this.router.url.includes(path);
    } 
}