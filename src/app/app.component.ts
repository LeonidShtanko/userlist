import { Component, OnInit } from '@angular/core';
import { User } from './models/user.model';
import { identifierModuleUrl } from '@angular/compiler';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
