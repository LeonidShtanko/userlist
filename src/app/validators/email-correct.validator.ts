import { FormControl } from '@angular/forms';

export function emailCorrectValidator(control: FormControl) {
    const value = control.value;

    const emailRegexp = /^([A-Z0-9._%\+\-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$)/i;

    const result = emailRegexp.test(value);

    if (result) {
        return null;
    } else {
        return {
            emaiValidator: {
                valid: false
            }
        };
    }
}