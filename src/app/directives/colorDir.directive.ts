import { Directive, ElementRef, OnInit, Renderer2, HostListener } from "@angular/core";

@Directive({
    selector: '[colorDir]'
})

export class ColorDirDirective implements OnInit {
    constructor(private item: ElementRef, private renderer: Renderer2){}

    @HostListener('click') public onElementClick() {
        alert('Directive click');
    }

    ngOnInit() {
        const tag: HTMLElement = this.item.nativeElement;
        // tag.style.color = 'red'

        this.renderer.setStyle(tag, 'color', 'darkblue');
    }
}