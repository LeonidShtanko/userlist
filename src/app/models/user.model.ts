export interface User {
    id: string;
    name: Name;
    email?: string;
    birth?: string;
    picture?: string;
}

export interface Name {
    first: string;
    last: string;
}