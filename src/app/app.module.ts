import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
// import { CardComponent } from './components/card/card.component';
import { DataService } from './services/data.service';
// import { UserInfoComponent } from './components/user-info/user-info.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { ListComponent } from './pages/list/list.component';
import { AboutComponent } from './pages/about/about.component';
// import { HeaderComponent } from './components/header/header.component';
import { AuthPageComponent } from './pages/auth/auth.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import { ComponentsModule } from './components/components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ColorDirDirective } from './directives/colorDir.directive';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListComponent,
    AboutComponent,
    AuthPageComponent,
    ColorDirDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    BrowserAnimationsModule
  ],
  providers: [
    DataService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
