import { Component, OnInit } from "@angular/core";
import { User } from 'src/app/models/user.model';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit{
    
    public users: User[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getUserList().subscribe(result => {
      this.users= result;
    })
  }

  public addUser(): void {
    const randomId = -0.5 + Math.random() * (10000);

    this.users.push({
      id: Math.round(randomId).toString(),
      name: {
        first: 'First',
        last: 'Last'
      },
      email: 'test@test.ru'
    });
  }

  public deleteUser(id: string): void {
    const user: User = this.users.find(u => u.id === id);
    const index = this.users.indexOf(user);
    this.users.splice(index, 1);
  }
  
}