import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailCorrectValidator } from 'src/app/validators/email-correct.validator';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})

export class AuthPageComponent implements OnInit {

    public email: FormControl;
    public password: FormControl;
    public authForm: FormGroup;

    constructor(private AuthService: AuthService, private router: Router) { }

    ngOnInit() {
        this.createFormFields();
        this.createForm();
    }

    public sendData(): void {
        if (this.authForm.valid) {
            this.AuthService.login(this.email.value, this.password.value);
            if (this.AuthService.isLogin) {
                this.router.navigate(['list']);
            }
        }
    }

    private createFormFields(): void {
        this.email = new FormControl('сюда то, что нужно высветить', [Validators.required, Validators.minLength(6), emailCorrectValidator]);
        this.password = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(10)]);
    }
    private createForm() : void {
        this.authForm = new FormGroup({
            email: this.email,
            password: this.password
        });
    }
}