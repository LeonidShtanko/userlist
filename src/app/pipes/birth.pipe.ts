import { PipeTransform, Pipe } from "@angular/core";
import *as moment from 'moment';

@Pipe({
    name: 'birthPipe'
})

export class BirthPipe implements PipeTransform {
    transform(value: string) {
        const dateTime = value.split('T'); //['1986-05-02', '18:25:17']
        // return moment.unix(value).format('LLL');
        return moment(dateTime[0], 'YYYY-MM-DD').format('LL');
    }
}
