import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()

export class AuthGuard implements CanActivate {

    constructor(private authservice: AuthService, private router: Router) { }

    canActivate() {
        const isLogin = this.authservice.isLogin;

        if(isLogin) {
            return true;
        } else {
            this.router.navigate(['auth']);
            return false;
        }
    }


}