import { Injectable } from "@angular/core";
import { User } from '../models/user.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { element } from '@angular/core/src/render3';

@Injectable()

export class DataService {
    // private _user: User;

    constructor(private http: HttpClient) {}

    public changeData: Subject<User> = new Subject();

    // public get user(): User {
    //     console.log('Get data to _user variable');
    //     return this._user;
    // }
    // public set user(value: User) {
    //     console.log('Set data to _user variable');
    //     this._user = value;
    // }

    public sendUserData(value: User): void {
        this.changeData.next(value);
    }
    public getUserList() {
        return this.http.get('https://randomuser.me/api/?results=10').pipe(map((data: any) =>{
            const users: User[] = [];
            data.results.forEach(element => {
                const user: User = {
                    id: element.id.value,
                    name: {
                        first: element.name.first,
                        last: element.name.last
                    },
                    email: element.email,
                    birth: element.dob.date,
                    picture: element.picture.thumbnail
                };
                users.push(user);
            });
            return users;
        }));
    }
}
